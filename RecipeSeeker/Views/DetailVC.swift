//
//  DetailVC.swift
//  RecipeSeeker
//
//  Created by Ariel Antonio Fundora López on 09/01/2018.
//  Copyright © 2018 Ariel Antonio Fundora López. All rights reserved.
//

import UIKit

class DetailVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var ivRecipe: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbIngredients: UILabel!
    @IBOutlet weak var btnOpenLink: UIButton!
    
    var recipe: Recipe!
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.ivRecipe.image = recipe.image
        self.lbTitle.text = recipe.title
        self.lbIngredients.text = "Ingredientes: \(recipe.ingredients)"
        
        self.btnOpenLink.layer.cornerRadius = 10
    }

    // MARK: - User Actions
    @IBAction func openLinkButtonTapped(_ sender: Any) {
        if let url = URL(string:recipe.href) {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
}
