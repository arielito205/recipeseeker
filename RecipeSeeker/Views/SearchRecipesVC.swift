//
//  SearchRecipesVC.swift
//  RecipeSeeker
//
//  Created by Ariel Antonio Fundora López on 09/01/2018.
//  Copyright © 2018 Ariel Antonio Fundora López. All rights reserved.
//

import UIKit

class SearchRecipesVC: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Outlets
    @IBOutlet weak var recipesSearchBar: UISearchBar!
    @IBOutlet weak var recipesTable: UITableView!
    // MARK: - Properties
    var recipes: [Recipe] = []
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //Register for keyboardNotifications
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        //
        self.recipesTable.rowHeight = UITableViewAutomaticDimension
        //Populate table
        performSearch()
    }

    // MARK: - UISearchBarDelegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.performSearch()
    }
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recipes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeCell", for: indexPath) as! RecipeCell
        cell.lbTitle.text = self.recipes[indexPath.row].title
        cell.ivThumbnail.image = self.recipes[indexPath.row].image
        return cell
    }
    
    // MARK: - Methods
    func performSearch()  {
        let params:[String:String] = ["q":self.recipesSearchBar.text!]
        NetworkManager.sharedInstance.getAsync(methodName: "api/", Parameters: params) { (isSuccess, networkResult) in
            if isSuccess {
                //Map the recipes
                let dict = networkResult as! [String:Any]
                if let results = dict["results"] as? Array<Any> {
                    var recipes:[Recipe] = []
                    for obj in results{
                        if let recipe = Recipe(json: obj as! [String : Any]) {
                            recipes.append(recipe)
                            //UpdateImage Image
                            if recipe.thumbnail != "" {
                                self.updateImage(recipe: recipe)
                            }
                        }
                    }
                    self.recipes = recipes
                    self.recipesTable.reloadData()
                }
            }
        }
    }
    
    func updateImage(recipe:Recipe)  {
        //DownloadImage
        NetworkManager.sharedInstance.getRemoteImage(imageURL:URL(string: recipe.thumbnail)!) { (success, networkResult) in
            if (success) {
                if let data = networkResult {
                    recipe.image = UIImage.init(data: data)!
                    self.recipesTable.reloadData()
                }
            }
        }
    }
    
    
    @objc func keyboardWillShow(_ notification:Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.recipesTable.contentInset = UIEdgeInsetsMake(0, 0, keyboardSize.height, 0)
        }
    }
    
    @objc func keyboardWillHide(_ notification:Notification) {
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            self.recipesTable.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetailView" {
            if let indexPath = self.recipesTable.indexPathForSelectedRow {
                let destinationVC = segue.destination as! DetailVC
                destinationVC.recipe = self.recipes[indexPath.row]
            }
        }
    }


}
