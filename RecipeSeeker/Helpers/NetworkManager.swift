//
//  NetwotkManager.swift
//  RecipeSeeker
//
//  Created by Ariel Antonio Fundora López on 09/01/2018.
//  Copyright © 2018 Ariel Antonio Fundora López. All rights reserved.
//

import UIKit
import Alamofire

class NetworkManager: NSObject {
    
    
    static let sharedInstance = NetworkManager()
    var baseRequestURL = "http://www.recipepuppy.com/"
    
    /**This method manage network asynchronous get request */
    func getAsync(methodName:String,Parameters dict:Parameters, completion: @escaping (_ success:Bool,_ networkResult: Any?)->Void) {
        let requestURL = baseRequestURL+methodName
        
        Alamofire.request(requestURL, method: .get, parameters: dict, encoding: URLEncoding.queryString).validate().responseJSON { (response) in
            if response.result.isSuccess { // handle success
                let resultValue = response.result.value
                completion(true, resultValue)
            }
            else { // handle failure
                print("Request failed")
                completion(false, nil)
            }
        }
    }
    
    func getRemoteImage(imageURL: URL , completion: @escaping (_ success:Bool,_ networkResult: Data?)->Void) {
        // Use Alamofire to download the image
        Alamofire.request(imageURL).responseData { (response) in
            if response.result.isSuccess {
                let resultValue = response.result.value
                completion(true, resultValue)
            }
            else {
                completion(false, nil)
            }
        }
    }
    
}






