//
//  Recipe.swift
//  RecipeSeeker
//
//  Created by Ariel Antonio Fundora López on 09/01/2018.
//  Copyright © 2018 Ariel Antonio Fundora López. All rights reserved.
//

import UIKit

class Recipe: NSObject {

    var title:String
    var href:String
    var ingredients: String
    var thumbnail: String
    var image: UIImage
    
    init(title:String, href:String, ingredients: String, thumbnail: String) {
        self.title = title
        self.href = href
        self.ingredients = ingredients
        self.thumbnail = thumbnail
        self.image = UIImage(named: "defaultThumbnail")!
    }
    
    //JSON init
    convenience init?(json: [String: Any]) {
        guard let title = json["title"] as? String
            ,let href = json["href"] as? String
            ,let ingredients = json["ingredients"] as? String
            ,let thumbnail = json["thumbnail"] as? String
            else {
                return nil
        }
        let unicodeTitle = String.init(htmlEncodedString: title)
        self.init(title: unicodeTitle!, href: href, ingredients: ingredients, thumbnail: thumbnail)
    }

}

